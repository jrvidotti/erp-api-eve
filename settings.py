MONGO_HOST = 'localhost'
MONGO_PORT = 27017
#MONGO_USERNAME = 'eve'
#MONGO_PASSWORD = 'eve'
MONGO_DBNAME = 'evedb'

SERVER_NAME = '0.0.0.0:5000'
URL_PROTOCOL = 'http'

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

pessoas = {
    'item_title': 'pessoas',
    'schema': {
        'nome': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 100,
            'required': True,
        },
        'cpf': {
            'type': 'string',
            'minlength': 11,
            'maxlength': 11,
            'required': True,
            'unique': True,
        },
        'sexo': {
            'type': 'string',
            'allowed': ['Masculino', 'Feminino'],
        },
        'data_nascimento': {
            'type': 'datetime',
        },
        'email': {
            'type': 'string',
        },
    }
}

DOMAIN = {
    'pessoas': pessoas,
}